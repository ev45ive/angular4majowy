import { Angular4majowyPage } from './app.po';

describe('angular4majowy App', () => {
  let page: Angular4majowyPage;

  beforeEach(() => {
    page = new Angular4majowyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
