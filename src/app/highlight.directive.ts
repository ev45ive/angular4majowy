import { Directive, ElementRef, Input, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[highlight]',
  // host:{
  //   '[style.borderLeftColor]':'color'
  // }
})
export class HighlightDirective {

  constructor() { }

  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.active? this.color : 'black'
  }
  
  active = false;

  @Input('highlight')
  color = 'black'

  @HostListener('mouseenter',['$event.target'])
  onmouseenter(target){
    this.active = true;
  }

  @HostListener('mouseleave')
  onmouseleave(){
    this.active = false;
  }

  ngOnInit(){
    //console.log(this.elem.nativeElement)

  }

}
