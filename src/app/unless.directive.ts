import { Directive, Input, OnChanges, TemplateRef,ViewContainerRef, EmbeddedViewRef, ViewRef } from '@angular/core';

@Directive({
  selector: '[unless]'
})
export class UnlessDirective implements OnChanges{

  cache: ViewRef;

  @Input('unless')
  set onChange(hide){

    if(hide){
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl)
      }
    }
  }

  constructor(private tpl:TemplateRef<any>, 
              private vcr:ViewContainerRef) {
   console.log('unless')   
  }

  ngOnChanges(changes){

  }

  ngOnDestroy(){
    this.cache.destroy()
  }
}
