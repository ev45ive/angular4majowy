import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'music-search',
  template: `
    <p>
      music-search Works!
    </p>
    <div class="row">
      <div class="col">
        <music-search-form></music-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <music-albums-list></music-albums-list>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
