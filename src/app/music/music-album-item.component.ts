import { Component, OnInit, Input } from '@angular/core';
import { IAlbum, IAlbumImage,IArtist } from './spotify'

@Component({
  selector: 'music-album-item',
  template: `
  <img class="card-img-top" [src]="image.url">
  <div class="card-block">
    <h4 class="card-title">{{album.name | shorten:24 }}</h4>
    <small>{{artist.name}}</small>
  </div>
  `,
  styles: [`
    :host{
      display:block;
      min-width: 33% !important;
    }
    img{
      width:100%;
    }
  `]
})
export class MusicAlbumItemComponent implements OnInit {

  @Input('album')
  set setAlbum(album:IAlbum){
    this.album = album
    this.image = album.images[0]
    this.artist = album.artists[0]
  }

  album:IAlbum;
  artist:IArtist;
  image:IAlbumImage;

  constructor() { }

  ngOnInit() {
  }

}
