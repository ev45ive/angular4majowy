import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { MusicSearchComponent } from './music-search.component';
import { MusicSearchFormComponent } from './music-search-form.component';
import { MusicAlbumsListComponent } from './music-albums-list.component';
import { MusicAlbumItemComponent } from './music-album-item.component';

import { MusicSearchService } from './music-search.service';
import { ShortenPipe } from './shorten.pipe'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent, 
    MusicSearchFormComponent, 
    MusicAlbumsListComponent, 
    MusicAlbumItemComponent, 
    ShortenPipe
  ],
  exports:[
    MusicSearchComponent
  ],
  providers:[
    MusicSearchService
  ]
})
export class MusicModule { }
