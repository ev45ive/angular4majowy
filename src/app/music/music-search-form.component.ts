import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from "./music-search.service";

import { FormGroup, FormControl, FormArray, Validators, ValidatorFn, ValidationErrors, AbstractControl, AsyncValidatorFn } from '@angular/forms'

// BOOTSTRAP WIP:
// [class.has-danger]="(queryForm.controls.query.touched || queryForm.controls.query.dirty) && queryForm.controls.query.invalid"
//  [class.form-control-danger]="queryForm.controls.query.invalid"

@Component({
  selector: 'music-search-form',
  template: `
  <form [formGroup]="queryForm">
    <div class="input-group" >
        <input class="form-control" formControlName="query">
        <input type="submit" value="Search" class="btn btn-success">
    </div>
    <div *ngIf="queryForm.pending">Proszę czekać....</div>

    <div *ngIf="queryForm.controls.query.touched || queryForm.controls.query.dirty">
      <div *ngIf="queryForm.controls.query.errors?.required">Pole jest wymagane!</div>
      <div *ngIf="queryForm.controls.query.errors?.not_value">
        Pole nie może zawierać {{queryForm.controls.query.errors?.not_value}}!  
      </div>
      <div *ngIf="queryForm.controls.query.errors?.minlength">
        Wymagane jest minimum {{ queryForm.controls.query.errors.minlength.requiredLength }} znaków
      </div>
    </div>
  </form>
  `,
  styles: [`
    input.ng-dirty.ng-invalid,
    input.ng-touched.ng-invalid {
        border: 1px solid red;
    }
  `]
})
export class MusicSearchFormComponent implements OnInit {

  queryForm:FormGroup

  constructor(private music:MusicSearchService) {


    const not_value = (text):ValidatorFn => (control:AbstractControl) => {

      return RegExp(text,'i').test(control.value)? { not_value:text } : null;
    }

    const async_not_value = (text):AsyncValidatorFn => (control:AbstractControl) => {
      return new Promise<ValidationErrors>( (resolve,reject)=>{

          setTimeout(()=>{
            resolve(  RegExp(text,'i').test(control.value)? { not_value:text } : null  )
          },2000)
        
      })
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('',[
        Validators.required,
        Validators.minLength(3),
        //not_value('alice')
      ],[
        async_not_value('alice')
      ]),
    })
    console.log(this.queryForm)

    this.queryForm
        .get('query')
        .valueChanges
        
        .debounceTime(400)
        .filter( query => query.length >= 3 )
        .distinctUntilChanged()

        .subscribe( query => {
          this.search(query)
        })
  }

  ngOnInit() {
  }

  search(query){
    this.music.search(query)
  }

}
