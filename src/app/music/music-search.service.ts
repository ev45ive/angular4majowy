import { Injectable } from '@angular/core';
import { IAlbum } from './spotify'
import { Http, Response } from '@angular/http'
import { Observable, Subject } from 'rxjs'

@Injectable()
export class MusicSearchService {

  albums:IAlbum[] = []

  albums$ = new Subject<IAlbum[]>()

  constructor(private http:Http) { 
    this.albums = JSON.parse(window.localStorage.getItem('albums'))
  }

  getAlbums$(){
    return this.albums$.startWith(this.albums)
  }

  search(query){
    let url = `https://api.spotify.com/v1/search?type=album&market=PL&query=${query}`
    this.http.get(url).subscribe( response => {
      let data =  response.json()
      this.albums = data.albums.items;

      if(this.albums.length)
      window.localStorage.setItem('albums', JSON.stringify(this.albums))

      this.albums$.next(this.albums)
    })

console.log(url)
  }



}
