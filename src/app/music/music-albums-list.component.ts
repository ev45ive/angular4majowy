import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from './music-search.service'

@Component({
  selector: 'music-albums-list',
  template: `
    <h3>Albums</h3>
    <div class="card-group">
      <music-album-item  class="card"
        *ngFor="let album of albums$ | async "
        [album]="album"
      ></music-album-item>
    </div>
  `,
  styles: []
})
export class MusicAlbumsListComponent implements OnInit {

  constructor(private music:MusicSearchService) { }

  albums$

  ngOnInit() {
    this.albums$ = this.music.getAlbums$()
    // .delay(2000)
    // .map( albums => {
    //   console.log('Filter')
    //   return albums.filter( album => album.name.length > 20 )
    // });

    // this.subcription = this.music.getAlbums$().subscribe( albums => {
    //   this.albums = albums;
    // })
  }

  // ngOnDestroy(){
  //   this.subcription.remove()
  // }
}
