export interface Spotify {}
// https://api.spotify.com/v1/search?type=album&market=PL&query=batman

interface IEntity{
    id: string
    name: string;
}

export interface IAlbum extends IEntity{
    artists: IArtist[]
    images: IAlbumImage[]
}

export interface IArtist extends IEntity{}

export interface IAlbumImage{
    url: string,
    width: number,
    height: number
}

// http://bit.ly/2kE4cZj