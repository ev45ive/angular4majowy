import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MusicModule } from './music/music.module'

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistsListItemComponent } from './playlists/playlists-list-item.component';
import { PlaylistsDetailsComponent } from './playlists/playlists-details.component';


import { PlaylistsService } from './playlists/playlists.service';
import { HighlightDirective } from './highlight.directive';
import { UnlessDirective } from './unless.directive'

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistsListItemComponent,
    PlaylistsDetailsComponent,
    HighlightDirective,
    UnlessDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MusicModule
  ],
  providers: [
   // { provide: PlaylistsService, useClass:TestPlaylistsService}
   PlaylistsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
