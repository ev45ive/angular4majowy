import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  //templateUrl: './app.component.html',
  //styleUrls: ['./app.component.css']
  template: `
    <div class="container">
     <h1>
      {{dane.nazwa}}
    </h1>
    <music-search></music-search>
  
  </div>    
  
  `,
  styles:[
    ` h1{ color: red } `
  ]
})
export class AppComponent {
  
  // counter = 0;

  // constructor(){
  //   setInterval(() => {
  //     this.counter++;
  //   },1000)
  // }

  dane = {
    nazwa: 'Angular App'
  }
}
