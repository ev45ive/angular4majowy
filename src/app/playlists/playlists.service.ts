import { Injectable, EventEmitter } from '@angular/core';
import {Playlist} from './interfaces'

@Injectable()
export class PlaylistsService {

  selected:Playlist;

  playlists:Playlist[] = [
    {
      id: 123,
      name:'Angular Greatest Hits',
      favourite: false,
      color: '#00ff00'
    },
    {
      id: 234,
      name:'Typescript Songs',
      favourite: false,
      color:'#0000ff'
    },
    {
      id: 345,
      name: 'Angular the Best Of!',
      favourite: true,
      color: '#ff0000'
    }
  ]

  selection = new EventEmitter<Playlist>()

  selectPlaylist(playlist){
    this.selected = playlist
    this.selection.emit(this.selected)
  }

  createPlaylist():Playlist{
    return {
      id: null, 
      name:'', 
      favourite: false, 
      color:'#000000'
    }
  }

  savePlaylist(playlist){
    if(playlist.id){
      // update
      const original = this.playlists.find( p => p.id === playlist.id)
      Object.assign( original, playlist )
    }else{
      // addNew
      playlist.id = Date.now()
      this.addPlaylist(playlist)
    }
  }

  addPlaylist(playlist){
    this.playlists.push(playlist)
  }

  constructor() {
    this.selected = this.playlists[0];
  }

}
