import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Inject } from '@angular/core';
import { Playlist } from './interfaces'
import { PlaylistsService } from './playlists.service'


        // [style.borderLeftColor]=" (hover == playlist ? playlist.color : '#fff' ) "
        // (mouseenter)=" hover = playlist "
        // (mouseleave)=" hover = false "

@Component({
  selector: 'playlists-list',
  template: `
  <ul class="list-group">
    <li class="list-group-item playlists-list-item " 
        *ngFor="let playlist of playlists trackBy track"
        [class.active]="active == playlist"
        (activate)=" onActivate($event)"
        (removed)=" onRemove($event)"
        [playlist]="playlist"


        [highlight]="playlist.color"
        >
    </li>
  </ul>
  `,
  styles:[`
    .playlists-list-item{
      border-left: 5px solid black
    }
  `]
})
export class PlaylistsListComponent implements OnInit {

  constructor(private service: PlaylistsService) {
    // var service = new PlaylistsService()
    this.playlists = service.playlists;
  }
  
  track(index,playlist:Playlist){
    return playlist.id
  }

  playlists: Playlist[] = []

  @Output()
  activate = new EventEmitter();

  active;

  onActivate(playlist) {
    this.activate.emit(playlist)
    this.service.selectPlaylist(playlist)
  }
  onRemove(playlist) {
    var index = this.playlists.indexOf(playlist)
    this.playlists.splice(index, 1)
  }
  ngOnInit() {
    this.active = this.service.selected
    this.service.selection.subscribe( playlist => {
      this.active = playlist
    })
  }

}
