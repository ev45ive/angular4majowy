import { Component, OnInit,Input } from '@angular/core';
import { PlaylistsService } from './playlists.service'
import {Playlist} from './interfaces'

@Component({
  selector: 'playlists-details',
  template: `
  <div [ngSwitch]="editting" *ngIf="playlist">

    <ng-content select="header"></ng-content>

    <div *ngSwitchDefault>
      <p>
        {{playlist.name}}
      </p>
      <p> {{  playlist.favourite? 'Ulubiona' : '' }}</p>
      <p style="width:50px;height:1em;" [style.backgroundColor]="playlist.color"></p>
      
      <button (click)=" edit()">Edit</button>
      <button (click)="create()">New</button>
    </div>

    <div *ngSwitchCase="true">
      <div>
        <label>Name:</label>
        <input type="text" [(ngModel)]="playlist.name">
      </div>
      <div>
        <label>Favourite:</label>
        <input type="checkbox" [(ngModel)]="playlist.favourite">
      </div>
      <div>
        <label>Color:</label>
        <input type="color" [(ngModel)]="playlist.color">    
      </div>

      <button (click)=" cancel() ">Cancel</button>
      <button (click)="save()">Save</button>
    </div>



    <ng-content select="footer"></ng-content>
  </div>
  `,
  styles: []
})
export class PlaylistsDetailsComponent implements OnInit {

  @Input()
  // set setPlaylist(playlist){
  //   this.playlist = Object.assign({},playlist)
  // }
  playlist:Playlist

  save() {
    this.service.savePlaylist(this.playlist)
   // this.create()
   this.service.selectPlaylist(this.playlist)
   this.editting = false;
  }

  editting = false

  edit(){
    this.editting = true;
    // make copy
    this.playlist = Object.assign({}, this.playlist)
  }
  cancel(){
    this.editting = false;
    // restore original
    this.playlist = this.service.selected;
  }

  create(){
    this.playlist = this.service.createPlaylist()
    this.editting = true
  }

  constructor(private service:PlaylistsService) { }

  subscription

  ngOnInit() {
    this.playlist = this.service.selected;

    this.subscription = this.service.selection.subscribe( playlist => {
      this.playlist = playlist
    })
  }

  ngOnDestroy(){
    this.subscription.remove()
  }

}
