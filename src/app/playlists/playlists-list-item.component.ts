import { Component, OnInit, Input, Output, EventEmitter, ElementRef, Renderer } from '@angular/core';
import {Playlist} from './interfaces'

@Component({
  selector: 'playlists-list-item,li.playlists-list-item',
  template: `
      {{playlist.name}}
      <button (click)="select()">Select</button>
      <button (click)="remove()">Delete</button>
  `,
  // inputs:[
  //   'playlist:data'
  // ],
  styles: []
})
export class PlaylistsListItemComponent implements OnInit {

  @Input()
  playlist:Playlist
  
  @Output('activate')
  activate = new EventEmitter<Playlist>()
 
  select(){
    this.activate.emit(this.playlist)
  }
  
  @Output()
  removed = new EventEmitter<Playlist>()
 
  remove(){
    this.removed.emit(this.playlist)
  }

  constructor(private renderer:Renderer,
            private elem : ElementRef
  ) {
    //console.log(elem, this.playlist)
  }

  ngOnInit() {

    // this.renderer.setElementStyle(
    //     this.elem.nativeElement,'color',this.playlist.color)
  }

  ngOnDestroy(){
   // console.log('destroy', this.playlist)
  }

}
